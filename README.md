[Link for Presenting](https://gitpitch.com/CalebProvost/derbydevops/master?grs=gitlab)

# Presentation Slides for [Derby DevOps](https://www.meetup.com/DevOps-Louisville/events/266533649/)



The speakers of Louisville's Derby DevOps are [Dennis Field](@ftab), Software Engineer at [Radio Sound Inc]([www.radiosound.com](http://www.radiosound.com/)), and [Zeff Morgan](@zeffmorgan), a Senior Software Engineer in Test at [GitLab, Inc](https://gitlab.com/gitlab-com/). Watch as they demonstrate how Continuous Integration, Continuous Delivery, Continuous Deployment is not just for websites. This demonstration shows how [Radio Sound Inc]([www.radiosound.com](http://www.radiosound.com/)) has used DevOps principles and applied them to IoT and embedded firmware scenarios; utilizing quick feedback loops to speed up the development process.



These slides are easily shared thanks to [GitPitch](https://gitpitch.com) a "new convention [which] automatically turns any **PITCHME.md** file found within a *GitLab* repository into a modern, responsive slide deck that is automatically available for sharing and presenting directly on [gitpitch.com](https://gitpitch.com)."