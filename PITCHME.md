## DevOps
## Beyond the Web 
### by GitLab and Radio Sound

---
@title[Introductions]
### Introductions

About Us

Note:
- These notes will only be visable for the presenter
- More words are here

---
@title[What is CI/CD?]
### Continuous Integration and Continuous Deployment

@ul[]
- Git
- Project management tools (JIRA, Trello)
- Code review
- Continuous integration
- Continuous Delivery
- Continuous Deployment
@ulend

Note:
- Take a quick poll to get a general understanding of the audience's knowledge of the topic

---
@title[Common Key Words]
### Common Key Words

@ul[]
- Issue
- Commit & Push
- Branch
- Merge
- Deploy
@ulend

Note:
- Issue: GitLab's way to track a feature request or bug report
- Commit/Push: in gross oversimplification, it's the "save" / "upload" button to write a log entry in the history book of this project, and share your work with the team. You'll always be able to refer back to it
- Branch: another copy of the code where you make changes while you're working on a feature or a fix. Your main branch might be where your solid tested code lives, and each new thing you want to add becomes a new branch
- Merge: feature is done (at least for now), branch goes back into the tree, back into your main code.
- Deploy: making that code run in a new environment or updating an existing one

---
@title[Common Key Words]
### Common Key Words

@ul[]
- Continuous Integration
- Continuous Delivery
- Continuous Deployment
@ulend

Note:
- Continuous integration: Always building code. Every commit is built, ideally auto tested, and your in-progress features and fixes are small, iterative, and merge back into your main branch quickly.
- Continuous delivery: Always delivering code up to the point it's ready to deploy
- Continuous deployment: Automatically deploying to production, not just staging. Key part of DevOps: getting features and fixes to the person using the product as quickly as possible so you can get feedback fast and have a better idea of what you really need to work on. We will be continuously deploying to staging, and manually deploying to production for demo purposes.


---
@title[Environment]
### Environment

@ul[]
- Environment
- Internet of Things (IoT)
@ulend

Note:
- "Production": your application's "live" or "stable" environment. This is what all end users ultimately get. This is the version that should get baked in at the factory floor, this is the version that things already in the field should be updated to, etc.
- "Staging": always updated with features and fixes you've merged. If you're not continuously deploying to production, you'll have one of these.
- "Review": a temporary environment set up just for testing one feature or one fix. Automatically created at the time work starts, automatically torn down when it's merged.
- You can go deeper. Working with another manufacturer? Multiple environments in the pipeline. Internal -> customer -> beta testers -> production

---
@title[Internet of Things (IoT)]
### Internet of Things (IoT)

@ul[]
- IoT
- Also Known as IoS
@ulend

Note:
- Anything other than your desktop or laptop computer that has an internet connection. Phones, tablets, cars these days have an internet connection, there are all sorts of little embedded computers living in every day things.
- Their operating systems are getting as sophisticated as your average desktop or laptop. But they aren't always set up right, secure, simple.
- The S in IoT stands for... well, there are some twitter feeds you do not want to end up on

---
@title[Challenges]
### Challenges

@ul[]
- State of the target (off, on, in use, locked up)
- Access to the target
- Control of target
- Updates
- Product Safety
- Resources
@ulend

Note:
- State of Target
- Access to target
- It's not as easy to deploy an update when you aren't in complete physical possession and control of the target device
- You can do your best to have the device download updates, push updates to it
- Even then, it's super easy to do it wrong and brick it
- These things have largely been solved for the web.

---
@title[Solutions]
### Solutions

@ul[]
- Utilize same principles the Web DevOps have created
- Forecast our own cloud
- Code to work with our limitations
@ulend

Note:
- We can follow these same principles and work around the challenges
- Can I tell you a secret about the cloud? There is no cloud. It's just someone else's computer.
- Unless you're running your own servers in your own datacenter with your own staff, you can't reach over and flip the on/off switch, you don't control the other things it's running, and whoever is operating the datacenter could very well yank out the power cord and cause that server to die. Ask me how I know.
- Deploying to the cloud is deploying to something you don't really control, once it's in someone's VM or someone's datacenter. You just have the illusion of control through all these well-beaten paths, scripts, tools.
- So instead of deploying to that thing we don't ultimately control, we're going to deploy to this thing we don't ultimately control, once it's in someone's living room or car

---
@title[Scenario Clarification]
## Fine Print

#### This demo is not
@ul[]
- app signing
- secure boot
- encryption
- automated testing
- actual app store deployment (for the sake of time, this demo will not use Fastlane, TestFlight, Play Store testing channels, releases, or any other 3rd party methods)
@ulend

Note:
- Don't go live with this code being deployed as-is or you'll end up on a twitter feed you don't want to be in. Give us a call or an email.

---
@title[Pre-CI/CD Scenario]
### Scenario Issue

- Device connectivity

Note:
- Show the initial connection to the device
- We don't know which one we're actually connected to
- Here's a bright idea: add a button to blink the LED

---
@title[Durring Scenario]
### Scenario Work

@ul[]
- Tracking?
- Issue Board
- Tags
- Merge Requests
@ulend

Note:
- Where are we going to track this feature request?
    - GitLab issue board is my favorite
- Create issue (one for firmware, one for android, one for iOS)
- Drag to "doing"
- If you've ever seen a kanban / agile board, like Trello, this is right along those lines
- Keep your whole team up to speed on what's in progress, done, etc.
- Start MRs for the feature request, push code
- Touch on automated testing

---
@title[Durring Scenario]
### Onto CI/CD

@ul[]
- Review/Test code
- Mobile App
- Firware Update
@ulend

Note:
- Set up one of the phones for the review version of the app (push "local deploy" button in Android or iOS merge request)
    - This is a pipeline for everything that we want to build, test, or deploy for every commit
    - Deployed this locally, since the Mac here is connected to the same network as this phone
    - If I'm out and about and don't have the option to deploy locally, I can also grab the update from the web once it finishes this pipeline.
- LED button doesn't do anything?
- Right, we have to install the test version of the firmware too

---
@title[Durring Scenario]
### Firmware CI/CD

@ul[]
- MR for ESP32 Firmware
- Firmware Pipline
- Test/End Use
- Merge
@ulend

Note:
- Open merge request for ESP32 firmware feature
- Show pipeline (this should be wrapping up by now ~4-5 minutes)
- Scan QR code for test firmware
- This will reboot to install the update
- We haven't finished this feature yet, so our other two environments are still running the old code
- A key principle in DevOps toolchain: the test / temporary / development environment should be automatically set up and should ideally be as close as possible to the production environment.
- Change looks OK, let's merge; Because we might have multiple things merging in if we're working on multiple things at once, we may want a staging area that always has the latest copy that we can check before deploying to production. Again, ideally, want continuous deployment, but that takes time and need to build confidence in the deploying and updating processes.

---
@title[Potential Problems]
## Potential Problems
#### It's not about how fast you can go,
#### but how well you handle things going wrong

@ul[]
- "Fix" not working out? Rollback?
- Crashes devices
- A/B testing
- Downgrading
@ulend

Note:
Real test isn't how fast you can go when things are going well, but how you handle when things go wrong.
- Let's say you've deployed a feature or fix and it isn't working out. It's causing new issues or something.
In the simplest case: you just roll back the deployment with this button. It stops delivering the new version, and devices that already upgraded will downgrade.
Doesn't work for App Store, doesn't work if you've set up your device to forbid downgrades
- Worst case scenario, your code crashes and your device can no longer work or get updates?
Ideally, your CI/CD has automated tests with devices it's physically connected to that check that and prevent it from being deployed in the first place.
- Mitigate this risk with support for local rollback in your update process. How does it work? You have some sort of self test that determines the device successfully updated. If it didn't pass, the device reverts back to the last version it was running the next time it reboots.
- Minimize the risk of the change: feature flags
Instead of changing feature A to B throughout, you just branch out and have the code do either one.
This helps with A/B testing, progressive (canary) delivery, reverting a feature after the code has been deployed.
This has been done for a long time in mobile devices. How many times have you seen "We regularly update the app, blah blah, fix bugs, new features. Just leave auto update on? Pretty please?" They're pushing new code all the time and rolling the flags out gradually.
- You can't directly roll back to an older version, and if you try to deploy an older version as new again, you otherwise might have App Store review holding up your plans to fix something for the end user.

---
@title[Wrap Up]
### Thank You

- Presentation and demo can be found at [GitLab.com/RadioSound](https://gitlab.com/radiosound)
 

Note:
- "show notes" link to presentation and repos on GitLab.com (QR code for those in the audience that have one, we'll post link to the meetup group)
- there will be readmes for those who want to do some homework
- accepting merge requests for features and fixes to the apps
DevOps@radiosound.com

---?color=black

---?color=black
